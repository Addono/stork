
import Exceptions.NoSessionException
import jason.Jason
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.sync.Mutex

/**
 * Created by Marcin Wisniowski (Nohus) on 09/02/2020.
 */

class SessionManager {

    private val sessions = mutableMapOf<String, Session>()
    private var sessionCount = 0
    private val jason = Jason()

    fun createSession(): String {
        sessionCount++
        val session = Session(
            sessionCount.toString(),
            RawMeasurements(),
            null,
            false,
            Mutex()
        )
        sessions += session.id to session
        println("Created session: ${session.id}")
        return session.id
    }

    fun addMeasurement(sessionId: String, measurement: String) {
        val session = sessions[sessionId] ?: throw NoSessionException()
        session.measurements.addMeasurement(measurement)
        println("Added measurement to session: $sessionId")
    }

    fun getPosition(sessionId: String): Position? {
        val session = sessions[sessionId] ?: throw NoSessionException()
        return session.latestFix
    }

    suspend fun run() {
        while (true) {
            sessions.values.filter { !it.isRunning }.forEach { session ->
                session.measurements.getNewContents()?.let { measurements ->
                    GlobalScope.launch {
                        session.isRunning = true
                        try {
                            jason.getPosition(measurements)?.let { position ->
                                println("Got position for session ${session.id}: $position")
                                session.latestFix = position
                            }
                        } catch (e: Exception) {
                            println("Failed to get position for session ${session.id}: ${e.message}")
                        }
                        session.isRunning = false
                    }
                }
            }
            delay(1000)
        }
    }
}

data class Session(
    val id: String,
    val measurements: RawMeasurements,
    var latestFix: Position?,
    var isRunning: Boolean,
    val lock: Mutex
)
