
import com.fasterxml.jackson.databind.SerializationFeature
import io.ktor.application.call
import io.ktor.features.*
import io.ktor.http.HttpHeaders
import io.ktor.http.HttpStatusCode
import io.ktor.jackson.jackson
import io.ktor.response.respond


internal fun applicationDefaultHeadersConfiguration(): DefaultHeaders.Configuration.() -> Unit {
    return {
        header(HttpHeaders.Server, "Storkingo v.0.0.1")
    }
}

internal fun applicationCompressionConfiguration(): Compression.Configuration.() -> Unit {
    return {
        gzip {
            priority = 1.0
        }
        deflate {
            priority = 1.0
        }
    }
}

internal fun applicationContentNegotiationConfiguration(): ContentNegotiation.Configuration.() -> Unit {
    return {
        jackson {
            enable(SerializationFeature.INDENT_OUTPUT)
            enable(SerializationFeature.CLOSE_CLOSEABLE)
            enable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
            enable(SerializationFeature.WRITE_DATE_KEYS_AS_TIMESTAMPS)
            disable(SerializationFeature.WRITE_DATE_TIMESTAMPS_AS_NANOSECONDS)
        }
    }
}

internal fun applicationCORSConfiguration(): CORS.Configuration.() -> Unit {
    return {
        anyHost()
    }
}

internal fun applicationStatusPagesConfiguration(): StatusPages.Configuration.() -> Unit {
    return {
        exception<Exceptions.NoSessionException> {
            call.respond(HttpStatusCode.NotFound, "Session not found")
        }
    }
}
