@file:Suppress("FunctionName")

import io.ktor.application.call
import io.ktor.locations.get
import io.ktor.response.respond
import io.ktor.routing.Route

/**
 * Created by Marcin Wisniowski (Nohus) on 24/04/19.
 */

fun Route.MainApi() {

    get<Paths.sessionStart> { params ->
        val session = sessionManager.createSession()
        call.respond(session)
    }

    data class MeasurementResponse(
        val message: String
    )

    get<Paths.postMeasurement> { params ->
        val session = params.session
        val measurements = params.measurement
        sessionManager.addMeasurement(session, measurements)
        call.respond(MeasurementResponse("Measurement added"))
    }

    data class PositionResponse(
        val position: Position?
    )

    get<Paths.getPosition> { params ->
        val session = params.session
        val position = sessionManager.getPosition(session)
        call.respond(PositionResponse(position))
    }
}
