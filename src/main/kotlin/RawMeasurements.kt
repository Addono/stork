/**
 * Created by Marcin Wisniowski (Nohus) on 09/02/2020.
 */

class RawMeasurements {

    private val contents = mutableListOf<String>()
    private var measurementsCount = 0
    private var readAt = 0

    init {
        contents += """
            |# 
            |# Header Description:
            |# 
            |# Version: v2.0.0.1
            |#
            |# Raw,ElapsedRealtimeMillis,TimeNanos,LeapSecond,TimeUncertaintyNanos,FullBiasNanos,BiasNanos,BiasUncertaintyNanos,DriftNanosPerSecond,DriftUncertaintyNanosPerSecond,HardwareClockDiscontinuityCount,Svid,TimeOffsetNanos,State,ReceivedSvTimeNanos,ReceivedSvTimeUncertaintyNanos,Cn0DbHz,PseudorangeRateMetersPerSecond,PseudorangeRateUncertaintyMetersPerSecond,AccumulatedDeltaRangeState,AccumulatedDeltaRangeMeters,AccumulatedDeltaRangeUncertaintyMeters,CarrierFrequencyHz,CarrierCycles,CarrierPhase,CarrierPhaseUncertainty,MultipathIndicator,SnrInDb,ConstellationType,AgcDb,CarrierFrequencyHz
            |#""".trimMargin().lines()
    }

    fun addMeasurement(line: String) {
        contents += line
        measurementsCount++
    }

    fun getNewContents(): String? {
        if (measurementsCount <= readAt) return null
        val contents = getContents()
        readAt = measurementsCount
        return contents
    }

    private fun getContents(): String? {
        if (measurementsCount <= 5) return null
        return contents.joinToString("\n") + "\n"
    }
}
