import dev.nohus.autokonfig.types.StringSetting

/**
 * Created by Marcin Wisniowski (Nohus) on 09/02/2020.
 */

object Settings {
    val key by StringSetting()
    val token by StringSetting()
}
