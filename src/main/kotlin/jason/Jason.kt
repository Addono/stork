package jason
import Position
import java.io.File

/**
 * Created by Marcin Wisniowski (Nohus) on 09/02/2020.
 */

class Jason {

    private val api = JasonApi()

    suspend fun test() {
        val id = uploadFile("test.txt")
        waitForProcess(id)
        val results = getResults(id)
        listResults(results)
        val positions = getPositions(results)
        printPositions(positions)
    }

    suspend fun getPosition(measurements: String): Position? {
        val id = uploadText(measurements, "filename.txt")
        waitForProcess(id)
        return getPositions(getResults(id)).lastOrNull()
    }

    private suspend fun uploadFile(filename: String): String {
        printFrameStart("File Upload")
        val file = File(filename)
        printMessage("Uploading ${file.name} (Size: ${file.length() / 1024} kB)")
        return uploadText(file.readText(), file.name).also {
            printMessage("Upload finished (process id: $it)")
            printFrameEnd()
        }
    }

    private suspend fun uploadText(text: String, filename: String): String {
        return api.uploadFile(text, filename).id
    }

    private suspend fun waitForProcess(id: String) {
        printFrameStart("Jason Processing")
        val messages = mutableListOf<String>()
        while (true) {
            val process = api.checkProcess(id)
            val newMessages = process.log.map { it.message }.filter { it !in messages }
            newMessages.forEach {
                printMessage(it)
            }
            messages += newMessages
            if (process.process.status in listOf("FINISHED", "ERROR")) break
        }
        printFrameEnd()
    }

    private suspend fun getResults(id: String): List<ProcessResult> {
        return api.checkProcess(id).results
    }

    private fun listResults(results: List<ProcessResult>) {
        printFrameStart("Result Files")
        results.forEach {
            printMessage("${it.name}: ${it.url}")
        }
        printFrameEnd()
    }

    private suspend fun getPositions(results: List<ProcessResult>): List<Position> {
        val url = results.firstOrNull { it.extension == "csv" }?.url ?: return emptyList()
        val result = api.downloadFile(url)
        return result.lines().drop(1).filterNot { it.isBlank() }.map {
            val columns = it.split(",")
            Position(columns[2], columns[3], columns[4])
        }
    }

    private fun printPositions(positions: List<Position>) {
        printFrameStart("Positions")
        positions.forEach {
            val link = getGoogleMapsLink(it)
            printMessage("Latitude: ${it.latitude}, Longitude: ${it.longitude}, Height: ${it.height}, Map: $link")
        }
        printFrameEnd()
    }

    private fun getGoogleMapsLink(position: Position): String {
        return "https://www.google.com/maps/search/?api=1&query=${position.latitude},${position.longitude}"
    }

    private fun printFrameStart(text: String) {
        val formatted = text.padEnd(16, ' ')
        println("┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┥ $formatted ┝━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━╾")
    }

    private fun printMessage(text: String) {
        println("┃ ${text.removeSuffix("\n")}")
    }

    private fun printFrameEnd() {
        println("┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━╾")
    }
}
