package jason
/**
 * Created by Marcin Wisniowski (Nohus) on 09/02/2020.
 */

data class UploadFileResponse(
    val message: String,
    val id: String
)

data class CheckProcessResponse(
    val process: ProcessStatus,
    val log: List<ProcessLog>,
    val results: List<ProcessResult>
)

data class ProcessStatus(
    val status: String
)

data class ProcessLog(
    val message: String
)

data class ProcessResult(
    val name: String,
    val extension: String,
    val url: String
)
