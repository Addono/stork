
class Exceptions {

    class NoSessionException(cause: Throwable? = null) : Exception(cause)

}
